package com.example.maks.todolist.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.maks.todolist.entity.TaskEntity;

import java.util.ArrayList;

public class TaskManager extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper";

    private static final String TABLE_NAME = "tasks";

    public TaskManager(Context context)
    {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                "id TEXT PRIMARY KEY, " +
                "name TEXT, " +
                "time TEXT, " +
                "date TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void save(TaskEntity entity)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", entity.getUuid());
        contentValues.put("name", entity.getName());
        contentValues.put("time", entity.getTime());
        contentValues.put("date", entity.getDate());
        db.replace(TABLE_NAME, null, contentValues);
    }

    public void delete(TaskEntity taskEntity)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "id = '" + taskEntity.getUuid() + "'", null);
    }

    public ArrayList<TaskEntity> findAll()
    {
        ArrayList<TaskEntity> listData = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        while (data.moveToNext())
        {
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setUuid(data.getString(data.getColumnIndex("id")));
            taskEntity.setName(data.getString(data.getColumnIndex("name")));
            taskEntity.setDate(data.getString(data.getColumnIndex("date")));
            taskEntity.setTime(data.getString(data.getColumnIndex("time")));
            listData.add(taskEntity);
        }
        return listData;
    }
}
