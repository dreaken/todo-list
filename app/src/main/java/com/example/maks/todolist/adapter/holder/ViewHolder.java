package com.example.maks.todolist.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.maks.todolist.R;

public class ViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    public final TextView name;
    public final TextView time;
    public final TextView date;

    public ViewHolder(View itemView) {
        super(itemView);
        this.view = itemView;
        this.name = this.view.findViewById(R.id.name);
        this.time = this.view.findViewById(R.id.time);
        this.date = this.view.findViewById(R.id.date);


    }
}
