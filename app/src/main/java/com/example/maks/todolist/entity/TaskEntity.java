package com.example.maks.todolist.entity;

import java.util.UUID;

public class TaskEntity {

    private String uuid = null;

    private String name = null;
    private String time = null;
    private String date = null;

    public TaskEntity(){
        this.setUuid(UUID.randomUUID().toString());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
