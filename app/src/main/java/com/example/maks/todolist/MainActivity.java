package com.example.maks.todolist;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.maks.todolist.adapter.TaskListAdapter;
import com.example.maks.todolist.entity.TaskEntity;
import com.example.maks.todolist.manager.TaskManager;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements AdapterView.OnClickListener {

    private static String TAG = "MainActivity";

    private TextView dateView;
    private TextView timeView;

    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;

    private RecyclerView tasks;
    private TaskListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.datePickersEvents();
        this.createNewEvent();
        this.populateListView();
    }

    private void deteItem(View view) {
        int position = tasks.indexOfChild((View) view.getParent());
        TaskEntity taskEntity = adapter.get(position);
        adapter.remove(position);

        TaskManager taskManager = new TaskManager(this);
        taskManager.delete(taskEntity);

        Snackbar.make(view, "Removed!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

    }

    private void createNewEvent()
    {
        Button addNew = findViewById(R.id.addNew);
        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TaskEntity taskEntity = new TaskEntity();
                Button addNew = findViewById(R.id.addNew);
                addNew.setClickable(false);
                EditText name = findViewById(R.id.newName);
                taskEntity.setName(name.getText().toString());

                TextView time = findViewById(R.id.timeView);
                taskEntity.setTime(time.getText().toString());

                TextView date = findViewById(R.id.dateView);
                taskEntity.setDate(date.getText().toString());
                TaskManager taskManager = new TaskManager(view.getContext());
                taskManager.save(taskEntity);
                adapter.add(taskEntity);
                addNew.setClickable(true);

                Snackbar.make(view, "Wpis dodany!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    private void populateListView() {
        TaskManager taskManager = new TaskManager(this);
        ArrayList<TaskEntity> listData = taskManager.findAll();
       // TaskListAdapter adapter = new TaskListAdapter(this, R.layout.list_layout, listData);

        this.tasks = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        this.tasks.setLayoutManager(mLayoutManager);

        adapter = new TaskListAdapter(listData);
        this.tasks.setAdapter(adapter);
    }

    private void datePickersEvents()
    {
        dateView = findViewById(R.id.dateView);
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        MainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,
                        month,
                        day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.DKGRAY));
                dialog.show();

            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = month + "/" + dayOfMonth + "/" + year;
                dateView.setText(date);
            }
        };


        timeView = findViewById(R.id.timeView);
        timeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(
                        MainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mTimeSetListener,
                        hour,
                        minute,
                        true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.DKGRAY));
                dialog.show();

            }
        });

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String date = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);
                timeView.setText(date);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.delete:
                deteItem(v);
                break;

        }
    }
}
