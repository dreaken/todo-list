package com.example.maks.todolist.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.maks.todolist.R;
import com.example.maks.todolist.adapter.holder.ViewHolder;
import com.example.maks.todolist.entity.TaskEntity;

import java.util.ArrayList;
import java.util.List;

public class TaskListAdapter extends RecyclerView.Adapter {
    private static final String TAG = "TaskListAdapter";

    private ArrayList<TaskEntity> tasks;

    private ViewHolder viewHolder;

    public TaskListAdapter(ArrayList<TaskEntity> tasks)
    {
        this.tasks = tasks;
    }

    public TaskEntity get(int position)
    {
        return tasks.get(position);
    }

    public void remove(int position)
    {
        tasks.remove(position);
        this.notifyItemRemoved(position);
    }

    public void add(TaskEntity entity)
    {
        tasks.add(0, entity);
        this.notifyDataSetChanged();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout, parent, false);
        this.viewHolder =  new ViewHolder(view);
        return  this.viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TaskEntity taskEntity = tasks.get(position);
        this.viewHolder.name.setText(taskEntity.getName());
        this.viewHolder.time.setText(taskEntity.getTime());
        this.viewHolder.date.setText(taskEntity.getDate());
    }

    @Override
    public int getItemCount() {
        if (tasks != null)
        {
            return  tasks.size();
        }
        return 0;
    }
}
